package com.khallware.recipes.health;

public class RecipesHealthCheck extends com.codahale.metrics.health.HealthCheck
{
	@Override
	protected Result check() throws Exception
	{
		return(Result.healthy());
	}
}
