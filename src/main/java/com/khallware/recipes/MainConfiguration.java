package com.khallware.recipes;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import javax.validation.Valid;


public class MainConfiguration extends Configuration
{
       @Valid
       @NotNull
       private DataSourceFactory factory = new DataSourceFactory();

       @JsonProperty("database")
       public void setDataSourceFactory(DataSourceFactory factory)
       {
               this.factory = factory;
       }

       @JsonProperty("database")
       public DataSourceFactory getDataSourceFactory()
       {
               return(factory);
       }
}
