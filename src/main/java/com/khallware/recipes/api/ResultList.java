package com.khallware.recipes.api;

import java.util.List;

public class ResultList<T> extends java.util.HashMap<String, List<T>>
{
	final private String name;

	public ResultList(String name)
	{
		this.put(name, new java.util.ArrayList<T>());
		this.name = name;
	}

	public void add(T item)
	{
		this.get(name).add(item);
	}
}
