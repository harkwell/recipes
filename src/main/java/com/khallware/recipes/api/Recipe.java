package com.khallware.recipes.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name = "recipe")
public class Recipe
{
	@Length(min=3, max=255)
	private String name;

	private long id;

	public Recipe() {}

	@Id
	public long getId()
	{
		return(id);
	}

	public void setId(long id)
	{
		this.id = id;
	}

	@JsonProperty
	public String getName()
	{
		return(name);
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
