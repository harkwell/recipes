package com.khallware.recipes;

import com.khallware.recipes.api.Recipe;
import com.khallware.recipes.health.RecipesHealthCheck;
import com.khallware.recipes.resources.Recipes;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.setup.Environment;
import io.dropwizard.setup.Bootstrap;

public class Main extends io.dropwizard.Application<MainConfiguration>
{
	private final HibernateBundle<MainConfiguration> hibernateBundle =
		new HibernateBundle<MainConfiguration>(Recipe.class) {
			@Override
			public DataSourceFactory getDataSourceFactory(
					MainConfiguration cfg)
			{
				return(cfg.getDataSourceFactory());
			}
	};
	private final MigrationsBundle<MainConfiguration> migrationsBundle =
		new MigrationsBundle<MainConfiguration>() {
			@Override
			public DataSourceFactory getDataSourceFactory(
					MainConfiguration cfg)
			{
				return(cfg.getDataSourceFactory());
			}
	};

	@Override
	public void initialize(Bootstrap<MainConfiguration> bootstrap)
	{
		super.initialize(bootstrap);
		bootstrap.addBundle(this.hibernateBundle);
		bootstrap.addBundle(this.migrationsBundle);
	}

	@Override
	public void run(MainConfiguration cfg, Environment env)
	{
		env.jersey().register(
			new Recipes(hibernateBundle.getSessionFactory()));
		env.healthChecks().register("recipes",new RecipesHealthCheck());
	}

	public static void main(String... args) throws Exception
	{
		new Main().run(args);
	}
}
