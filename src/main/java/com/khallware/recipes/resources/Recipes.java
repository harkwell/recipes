package com.khallware.recipes.resources;

import com.khallware.recipes.api.Recipe;
import com.khallware.recipes.api.ResultList;
import com.khallware.recipes.dbase.RecipeDAO;
import com.codahale.metrics.annotation.Timed;
import io.dropwizard.hibernate.UnitOfWork;
import org.hibernate.SessionFactory;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.List;
import java.util.Optional;


@Path("/recipes")
@Produces(MediaType.APPLICATION_JSON)
public class Recipes
{
	private static RecipeDAO dao = null;

	public Recipes(SessionFactory sf)
	{
		dao = new RecipeDAO(sf);
	}

	@POST
	@UnitOfWork
	@Timed(name = "recipe-posts")
	public Recipe postRecipe(@Valid Recipe recipe)
	{
		dao.save(recipe);
		return(recipe);
	}

	@GET
	@UnitOfWork
	@Timed(name = "recipe-list-gets")
	public ResultList<Recipe> getRecipes(
			@QueryParam("index") Optional<Integer> idx,
			@QueryParam("pagesize") Optional<Integer> pagesize)
	{
		ResultList<Recipe> retval = new ResultList<>("recipes");
		retval.get("recipes").addAll(dao.getRecipes());
		return(retval);
	}

	@GET
	@UnitOfWork
	@Path("/{name}")
	@Timed(name = "recipe-gets")
	public Recipe getRecipe(@PathParam("name") String name)
	{
		List<Recipe> rslt = dao.get(name);
		return((rslt.size() > 0) ? rslt.get(0) : new Recipe());
	}

	@PUT
	@UnitOfWork
	@Timed(name = "recipe-puts")
	public Recipe putRecipe(@Valid Recipe recipe)
	{
		dao.save(recipe);
		return(recipe);
	}

	@DELETE
	@UnitOfWork
	@Path("/{name}")
	@Timed(name = "recipe-deletes")
	public Recipe deleteRecipe(@PathParam("name") String name)
	{
		return(dao.delete(name));
	}
}
