package com.khallware.recipes.dbase;

import com.khallware.recipes.api.Recipe;
import com.khallware.recipes.api.ResultList;
import io.dropwizard.hibernate.AbstractDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.hibernate.query.Query;
import org.hibernate.SessionFactory;


public class RecipeDAO extends AbstractDAO<Recipe>
{
	public RecipeDAO(SessionFactory sf)
	{
		super(sf);
	}

	public Optional<Recipe> get(int id)
	{
		return(Optional.<Recipe>ofNullable(super.get(id)));
	}

	public List<Recipe> get(String name)
	{
		List<Recipe> retval = null;
		String HQL = String.format(
			"FROM Recipe WHERE name = '%s'", name);
		try {
			retval = super.list(super.query(HQL));
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		return(retval);
	}

	public List<Recipe> getRecipes()
	{
		List<Recipe> retval = super.list(super.query("FROM Recipe"));
		return(retval);
	}

	public Recipe save(Recipe recipe)
	{
		super.persist(recipe);
		super.currentSession().flush();
		return(recipe);
	}

	public Recipe delete(String name)
	{
		Recipe retval = null;
		List<Recipe> rslt = get(name);

		if (rslt.size() > 0) {
			retval = rslt.get(0);
			super.currentSession().delete(retval);
			super.currentSession().flush();
		}
		return(retval);
	}
}

