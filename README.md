Khallware (Recipes)
=================
Overview
---------------
Food recipe web service and android front-end.

Build
---------------

```shell
export RECIPES_MAVEN_REPO=/tmp/foo
mvn -Dmaven.repo.local=$RECIPES_MAVEN_REPO package
rm -rf $RECIPES_MAVEN_REPO
```

One-Time
---------------

```shell
### setup miriadb instance
#eval $(minikube docker-env)
docker images |grep ware-mariadb.*v1.0 >/dev/null || echo 'BUILD IMAGE FIRST'

TOPDIR=$(mktemp -d)
cat <<EOF >$TOPDIR/Dockerfile
FROM centos
RUN yum install -y epel-release \
   && yum install -y mariadb mariadb-server
CMD /usr/bin/mysqld_safe
EXPOSE 3306
EOF
cd $TOPDIR && docker build --no-cache -t khallware-mariadb:v1.0 .
cd .. && rm -rf $TOPDIR/

### provision database files
ls -ld /tmp/khallware-mariadb || echo 'PROVISION DATABASE FIRST'

mkdir -p /tmp/khallware-mariadb
docker run -it -h mariadb --name khallware-mariadb -v /tmp/khallware-mariadb:/var/lib/mysql khallware-mariadb:v1.0 bash
mysql_install_db --user=mysql --ldata=/var/lib/mysql/
/usr/bin/mysqld_safe &
mysql -uroot mysql
CREATE DATABASE recipes;
CREATE USER 'api'@'%' IDENTIFIED BY 'khallware';
GRANT ALL PRIVILEGES ON recipes.* TO 'api'@'%' WITH GRANT OPTION;
USE mysql;
SET PASSWORD FOR 'api'@'%' = PASSWORD('khallware');
exit
exit
docker rm $(docker ps -a |grep mariadb |cut -d\  -f1)
```

Execution
---------------

```shell
### start the mariadb container
docker run -d -h mariadb --name khallware-mariadb -p 3306:3306 -v /tmp/khallware-mariadb:/var/lib/mysql khallware-mariadb:v1.0
echo 'SHOW TABLES;' |mysql -uapi -pkhallware -h 127.0.0.1 recipes


### start the application server
cat <<EOF >recipe.yaml
database:
   driverClass: org.mariadb.jdbc.Driver
   user: api
   password: khallware
   url: jdbc:mariadb://127.0.0.1:3306/recipes
   properties:
      hibernate.hbm2ddl.auto: update

server:
   applicationConnectors:
      - type: http
        port: 8080
   adminConnectors:
      - type: http
        port: 8081

#logging:
#   level: INFO
#   loggers:
#      com.khallware.recipes: DEBUG
#      org.hibernate.SQL:
#         level: DEBUG
#   appenders:
#      - type: console
#      - type: file
#         logFormat: "[%d{HH:mm:ss}] %-6level %logger{5} - %X{code} %msg %n"
#         currentLogFilename: /tmp/recipes.log
#         archivedLogFilenamePattern: /tmp/%d{yyyy-MM-dd}-%i-recipes.log.gz
#         archivedFileCount: 2
#         timeZone: CST
#         maxFileSize: 10MB
EOF

# startup
java -jar target/recipes-0.1.1.jar server recipe.yaml
echo 'DESCRIBE recipe;' |mysql -uapi -pkhallware -h 127.0.0.1 recipes

# test
curl -X POST -H "Content-Type: application/json" -d '{ "name":"recipe1" }' http://localhost:8080/recipes/
echo 'SELECT * FROM recipe;' |mysql -uapi -pkhallware -h 127.0.0.1 recipes
curl http://localhost:8080/recipes/
curl http://localhost:8080/recipes/recipe1
curl -X DELETE http://localhost:8080/recipes/recipe1
echo 'SELECT * FROM recipe;' |mysql -uapi -pkhallware -h 127.0.0.1 recipes
```

Cleanup
---------------

```shell
docker rm -f khallware-mariadb
docker rmi $(docker images |grep khallware-mariadb |awk '{print $3}')
sudo rm -rf /tmp/khallware-mariadb $RECIPES_MAVEN_REPO
```

Developer Resources
---------------
http://www.dropwizard.io/
https://docs.jboss.org/hibernate/validator/6.0/api/
